﻿using System;
using System.Collections.Generic;
using System.Text;
using TestAssignment.BL.Abstraction;

namespace TestAssignment.BL.Impl
{
    public class ElementMerger : IElementMerger
    {
        private static readonly ElementMerger _instance = new ElementMerger();

        private ElementMerger()
        {

        }


        public static IElementMerger Get()
        {
            return _instance;
        }

        public IEnumerable<IElement> MergeElements(IEnumerable<IElement> elements, IElement newElement)
        {
            throw new NotImplementedException();
        }
    }
}
